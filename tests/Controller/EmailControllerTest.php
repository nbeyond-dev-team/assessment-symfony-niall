<?php
declare(strict_types=1);

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class EmailControllerTest extends WebTestCase
{
    public function testStatusCode()
    {
        $client = static::createClient();
        $client->request('GET', '/email/send');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }

    public function testTitle()
    {
        $client = static::createClient();
        $client->request('GET', '/email/send');

        $this->assertSelectorTextContains('body > div.example-wrapper > h1', 'Email sent!');
    }
}
