<?php
declare(strict_types=1);

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class NotifyControllerTest extends WebTestCase
{
    public function testStatusCode()
    {
        $client = static::createClient();
        $client->request('GET', '/notify');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }

    public function testFormSubmission()
    {
        $client = static::createClient();
        $client->request('GET', '/notify');

        $crawler = $client->submitForm('Send');
        // NOTE: need to replace localhost with app URL
        $this->assertEquals('http://localhost/email/send', $crawler->getUri());
    }
}
