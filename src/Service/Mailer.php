<?php
declare(strict_types=1);

namespace App\Service;

use Psr\Log\LoggerInterface;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;

class Mailer
{
    private MailerInterface $mailer;
    private LoggerInterface $logger;

    /**
     * Mailer constructor.
     * @param \Symfony\Component\Mailer\MailerInterface $mailer
     * @param \Psr\Log\LoggerInterface $logger
     */
    public function __construct(MailerInterface $mailer, LoggerInterface $logger)
    {
        $this->mailer = $mailer;
        $this->logger = $logger;
    }

    /**
     * @param string $from
     * @param string $to
     * @param string $subject
     * @param string $text
     */
    public function send(
        string $from = 'foo@example',
        string $to = 'bar@example.com',
        string $subject = 'Subject',
        string $text = 'I am the email'
    ) {
        // NOTE: this is only an example service.
        // It would need to be able to send to multiple recipients, CC, BCC, attachments etc.

        $email = new Email();

        $email->from($from)->to($to)->subject($subject)->text($text);

        try {
            $this->mailer->send($email);
        } catch (TransportExceptionInterface $e) {
            $this->logger->error($e->getMessage());
        }
    }
}
