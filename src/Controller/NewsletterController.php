<?php

namespace App\Controller;

use App\Form\SendEmailType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class NewsletterController extends AbstractController
{
    /**
     * @Route("/newsletter", name="newsletter")
     */
    public function index(): Response
    {
        $form = $this->createForm(SendEmailType::class, null, ['action' => $this->generateUrl('email_send')]);

        return $this->render('newsletter/index.html.twig', ['form' => $form->createView()]);
    }
}
