<?php

namespace App\Controller;

use App\Service\Mailer;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class EmailController extends AbstractController
{
    /**
     * @Route("/email/send", name="email_send")
     * @param \App\Service\Mailer $mailer
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function send(Mailer $mailer): Response
    {
        $mailer->send();

        return $this->render('email/index.html.twig');
    }
}
