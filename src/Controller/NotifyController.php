<?php

namespace App\Controller;

use App\Form\SendEmailType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class NotifyController extends AbstractController
{
    /**
     * @Route("/notify", name="notify")
     */
    public function index(): Response
    {
        $form = $this->createForm(SendEmailType::class, null, ['action' => $this->generateUrl('email_send')]);

        return $this->render('notify/index.html.twig', ['form' => $form->createView()]);
    }
}
